FROM python:3.7-alpine

RUN apk add --update \
    protobuf \
    g++

RUN pip install --upgrade pip \
    	grpcio-tools \
    	mypy-protobuf

WORKDIR /app
