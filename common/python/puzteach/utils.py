import protos.env_state_pb2 as pt_protos

def get_object_index_by_id(game_state, id):
  for key, obj in enumerate(game_state.objects):
    if id == obj.object_id:
      return key
  raise Exception("Object ID not found")

def get_object_at(game_state, position):
  for obj in game_state.objects:
    if obj.position.x == position.x and obj.position.y == position.y:
      return obj
  return None

# Modifies a game state by applying a game state delta to it.
def advance_inplace(game_state, game_state_delta):
  # The actor data is simply stomped
  game_state.actor.CopyFrom(game_state_delta.actor)

  for obj_update in game_state_delta.objects:
    if obj_update.change == ADDED:
      game_state.objects.add().CopyFrom(obj_update.new_state)

    elif obj_update.change == UPDATED:
      obj_id = get_object_by_id(game_state, obj_update.new_state.object_id)
      game_state.objects[obj_id].CopyFrom(obj_update.new_state)

    elif obj_update.change == REMOVED:
      obj_id = get_object_by_id(game_state, obj_update.new_state.object_id)

      del game_state.objects[obj_id]
    else:
      assert False

  # actions are ignored for now
  game_state.finished = game_state_delta.finished

def get_forward_position(position, direction):
  result = pt_protos.Vec2()
  result.CopyFrom(position)

  if direction == pt_protos.TOP:
    result.y -= 1

  if direction == pt_protos.LEFT:
    result.x -= 1

  if direction == pt_protos.BOTTOM:
    result.y += 1

  if direction == pt_protos.RIGHT:
    result.x += 1
  
  return result

def rotate_direction(clockwise, direction):
  if direction == pt_protos.TOP:
    return pt_protos.RIGHT if clockwise else pt_protos.LEFT

  if direction == pt_protos.RIGHT:
    return pt_protos.BOTTOM if clockwise else pt_protos.TOP

  if direction == pt_protos.BOTTOM:
    return pt_protos.LEFT if clockwise else pt_protos.RIGHT

  if direction == pt_protos.LEFT:
    return pt_protos.TOP if clockwise else pt_protos.BOTTOM

  assert False
