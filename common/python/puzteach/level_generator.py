import protos.env_state_pb2 as pb
import random
import numpy as np
import copy

class LevelBuilder:
    def __init__(self):
        self.next_id = 1
        self.result = pb.GameState()

    def add_wall(self, x, y):
        cell = self.result.cells.add()
        cell.type = pb.WALL
        cell.position.x = x
        cell.position.y = y

    def add_object(self, x, y, obj_type, color):
        obj = self.result.objects.add()
        obj.object_id = self.next_id
        self.next_id += 1

        obj.type = obj_type
        obj.color = color
        obj.position.x = x
        obj.position.y = y

def generate_level(config):
    builder = LevelBuilder()
    result = builder.result

    result.level_size.x = config.level_size.x 
    result.level_size.y = config.level_size.y

    mx = config.level_size.x 
    my = config.level_size.y   # width and height of the maze

    maze = [[0 for x in range(mx)] for y in range(my)]
    dx = [0, 1, 0, -1]
    dy = [-1, 0, 1, 0]  # 4 directions to move in the maze
    color = [(30, 30, 30), (119, 119, 119)]  # RGB colors of the maze
    # start the maze from a random cell
    cx = random.randint(0, mx - 1)
    cy = random.randint(0, my - 1)

    maze[cy][cx] = 1
    stack = [(cx, cy, 0)]  # stack element: (x, y, direction)

    while len(stack) > 0:
        (cx, cy, cd) = stack[-1]

        # to prevent zigzags:
        # if changed direction in the last move then cannot change again
        if len(stack) > 2:
            if cd != stack[-2][2]:
                dirRange = [cd]
            else:
                dirRange = range(4)
        else:
            dirRange = range(4)

        # find a new cell to add
        nlst = []  # list of available neighbors

        for i in dirRange:
            nx = cx + dx[i]
            ny = cy + dy[i]
            if nx >= 0 and nx < mx and ny >= 0 and ny < my:
                if maze[ny][nx] == 0:
                    ctr = 0  # of occupied neighbors must be 1
                    for j in range(4):
                        ex = nx + dx[j]
                        ey = ny + dy[j]
                        if ex >= 0 and ex < mx and ey >= 0 and ey < my:
                            if maze[ey][ex] == 1:
                                ctr += 1
                    if ctr == 1:
                        nlst.append(i)

        # if 1 or more neighbors available then randomly select one and move
        if len(nlst) > 0:
            ir = nlst[random.randint(0, len(nlst) - 1)]
            cx += dx[ir]
            cy += dy[ir]
            maze[cy][cx] = 1
            stack.append((cx, cy, ir))
        else:
            stack.pop()

    # define walls and object based on the created maze


    walls = []
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == 0:
                walls.append([i, j])

    print("hi there")
    result.actor.position.x = cx
    result.actor.position.y = cy
    direction_list = [0, 1, 2, 3]
    result.actor.direction = random.choice(direction_list)
    # for j in range(rooms.rooms_count.y-1):
    #     for i in range(result.level_size.x):
    #         builder.add_wall(i, (j+1) * rooms.room_size.y)

    for wall in walls:
        builder.add_wall(wall[1], wall[0])

    Obj_end = []
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == 1:
                Obj_end.append([i, j])

    Obj_end = np.array(Obj_end)
    randomRow = np.random.randint(len(Obj_end), size=1)
    Exittt= Obj_end[randomRow, :]

    builder.add_object(Exittt[0, 1], Exittt[0, 0], pb.EXIT, pb.GREY)

    return result


