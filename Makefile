SHELL:=/bin/sh
ASSETS_ROOT := frontend/react/app/src/assets/img
REGISTRY_IMAGE := registry.gitlab.com/age-of-minds/puzzle-teach




build: protos/env_state.proto
	docker-compose run shell /bin/sh -c "\
		protoc --js_out=import_style=commonjs,binary:./frontend/react/app/src $< \
		&& protoc --python_out=./common/python/ $< \
		&& sed -i '1s;^;/* eslint-disable */\n;' ./frontend/react/app/src/protos/env_state_pb.js"

images: $(ASSETS_ROOT)/*.svg
	for i in $^ ; do \
		mv $$i $${i/GraphicalElements_/} ; \
	done

env_svc: Dockerfile.environment
	docker build -f $< --cache-from ${REGISTRY_IMAGE}/environment:latest -t ${REGISTRY_IMAGE}/environment .

agent_svc: Dockerfile.agent
	docker build -f $< --cache-from ${REGISTRY_IMAGE}/agent:latest -t ${REGISTRY_IMAGE}/agent .	
