__author__ = "Neda Navidi"
__copyright__ = "Copyright 2019, The Puzzle-Teach Project"
__credits__ = ["Neda Navidi",
               " ...."]
__Company__ = "Age of minds"
__version__ = "1.0.2"
__maintainer__ = "...."
__email__ = "neda@ageofminds.com"
__status__ = "demonstration"
__NAME__ = "FindExit"
__LEVEL__ = "0_17"
########################################################

import sys
import os.path
import base64
import random
import numpy as np

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../../common/python'))

import protos.env_state_pb2 as pb


class LevelBuilder:
    def __init__(self):
        self.next_id = 1
        self.result = pb.GameState()

    def add_wall(self, x, y):
        cell = self.result.cells.add()
        cell.type = pb.WALL
        cell.position.x = x
        cell.position.y = y

    def add_object(self, x, y, obj_type, color):
        obj = self.result.objects.add()
        obj.object_id = self.next_id
        self.next_id += 1

        obj.type = obj_type
        obj.color = color
        obj.position.x = x
        obj.position.y = y


def generate_rooms(config, room_size):
    builder = LevelBuilder()
    result = builder.result

    # result.level_size.x = rooms.rooms_count.x - 1 + rooms.rooms_count.x * rooms.room_size.x
    result.level_size.x = room_size.x
    # result.level_size.y = rooms.rooms_count.y - 1 + rooms.rooms_count.y * rooms.room_size.y
    result.level_size.y = room_size.y

    mx = tmp_config.room_size.x
    my = tmp_config.room_size.x  # width and height of the maze

    # Start to create a random and tough maze
    maze = [[0 for x in range(mx)] for y in range(my)]
    dx = [0, 1, 0, -1]
    dy = [-1, 0, 1, 0]  # 4 directions to move in the maze
    color = [(30, 30, 30), (119, 119, 119)]  # RGB colors of the maze
    # start the maze from a random cell
    cx = random.randint(0, mx - 1)
    cy = random.randint(0, my - 1)

    maze[cy][cx] = 1
    stack = [(cx, cy, 0)]  # stack element: (x, y, direction)

    while len(stack) > 0:
        (cx, cy, cd) = stack[-1]

        # to prevent zigzags:
        # if changed direction in the last move then cannot change again
        if len(stack) > 2:
            if cd != stack[-2][2]:
                dirRange = [cd]
            else:
                dirRange = range(4)
        else:
            dirRange = range(4)

        # find a new cell to add
        nlst = []  # list of available neighbors

        for i in dirRange:
            nx = cx + dx[i]
            ny = cy + dy[i]
            if nx >= 0 and nx < mx and ny >= 0 and ny < my:
                if maze[ny][nx] == 0:
                    ctr = 0  # of occupied neighbors must be 1
                    for j in range(4):
                        ex = nx + dx[j]
                        ey = ny + dy[j]
                        if ex >= 0 and ex < mx and ey >= 0 and ey < my:
                            if maze[ey][ex] == 1:
                                ctr += 1
                    if ctr == 1:
                        nlst.append(i)

        # if 1 or more neighbors available then randomly select one and move
        if len(nlst) > 0:
            ir = nlst[random.randint(0, len(nlst) - 1)]
            cx += dx[ir]
            cy += dy[ir]
            maze[cy][cx] = 1
            stack.append((cx, cy, ir))
        else:
            stack.pop()

    # paint the maze
    # for ky in range(imgy):
    #     for kx in range(imgx):
    #         pixels[kx, ky] = color[maze[int(my * ky / imgy)][int(mx * kx / imgx)]]
    # image.save("Maze_" + str(mx) + "x" + str(my) + ".png", "PNG")
    # print("generated maze", maze)
    # print("start_in generation", cx, cy)

    # define walls and object based on the created maze

    walls = []
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == 0:
                walls.append([i, j])

    result.actor.position.x = cx
    result.actor.position.y = cy
    direction_list = [0, 1, 2, 3]
    result.actor.direction = random.choice(direction_list)

    for wall in walls:
        builder.add_wall(wall[1], wall[0])

    Obj_end = []
    for i in range(len(maze)):
        for j in range(len(maze[i])):
            if maze[i][j] == 1:
                Obj_end.append([i, j])


    Obj_end = np.array(Obj_end)
    randomRow = np.random.randint(len(Obj_end), size=1)
    Exittt = Obj_end[randomRow, :]

    builder.add_object(Exittt[0, 1], Exittt[0, 0], pb.EXIT, pb.GREY)

    return result


def generate_level(config):
    if config.HasField('room_size'):
        return generate_rooms(config, config.room_size)


tmp_config = pb.LevelConfig()
tmp_config.room_size.x = 17
tmp_config.room_size.y = 17

# tmp_config.rooms_count.x = 1
# tmp_config.rooms_count.y = 1

lvl = generate_level(tmp_config)

print(base64.b64encode(lvl.SerializeToString()))


data = lvl
