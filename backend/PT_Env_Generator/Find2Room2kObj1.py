__author__ = "Neda Navidi"
__copyright__ = "Copyright 2019, The Puzzle-Teach Project"
__credits__ = ["Neda Navidi",
               " ...."]
__Company__ = "Age of minds"
__version__ = "1.0.1"
__maintainer__ = "...."
__email__ = "neda@ageofminds.com"
__status__ = "demonstration"
__NAME__ = "FindExit2Room2k"
__LEVEL__ = "2_2-1"
########################################################
import sys
import os.path
import base64
import random

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../../common/python'))

import protos.env_state_pb2 as pb


class LevelBuilder:
    def __init__(self):
        self.next_id = 1
        self.result = pb.GameState()

    def add_wall(self, x, y):
        cell = self.result.cells.add()
        cell.type = pb.WALL
        cell.position.x = x
        cell.position.y = y

    def add_object(self, x, y, obj_type, color):
        obj = self.result.objects.add()
        obj.object_id = self.next_id
        self.next_id += 1

        obj.type = obj_type
        obj.color = color
        obj.position.x = x
        obj.position.y = y


def generate_rooms(config, room_size):
    builder = LevelBuilder()
    result = builder.result

    # result.level_size.x = rooms.rooms_count.x - 1 + rooms.rooms_count.x * rooms.room_size.x
    result.level_size.x = room_size.x
    # result.level_size.y = rooms.rooms_count.y - 1 + rooms.rooms_count.y * rooms.room_size.y
    result.level_size.y = room_size.y

    mx = tmp_config.room_size.x
    my = tmp_config.room_size.x  # width and height of the maze


    # start the maze from a random cell
    cx = random.randint(1, (mx / 2) - 1)
    cy = random.randint(1, my - 2)

    Ex = random.randint((mx / 2) + 1, mx - 2)
    Ey = random.randint(1, my - 2)
    Exit_ = [Ex, Ey]

    ### Margins
    wall0 = []
    for i in range(mx):
        wall0.append([i, 0])
        wall0.append([i, mx - 1])

    for j in range(my):
        wall0.append([0, j])
        wall0.append([my - 1, j])

    # real wall
    wall = []
    for i in range(1, my - 1):  # 1 to 16
        wall.append([i, (mx - 1) / 2])

    doorR = random.choice(wall)
    doorR = [doorR[1], doorR[0]]

    while True:
        keyR = [random.randint(1, (mx / 2) - 1), random.randint(1, my - 2)]

        if ([cx, cy] != keyR and wall != keyR): break

    while True:
        keyB = [random.randint(1, (mx / 2) - 1), random.randint(1, my - 2)]
        if ([cx, cy] != keyR and keyB != keyR and wall != keyB): break

    while True:
        Obj1 = [random.randint(1, (mx / 2) - 1), random.randint(1, my - 2)]
        if ([cx, cy] != Obj1 and keyR != Obj1 and keyB != Obj1): break

    ##############################################################################################################

    walls = wall
    walls.extend(wall0)

    result.actor.position.x = cx
    result.actor.position.y = cy
    direction_list = [0, 1, 2, 3]
    result.actor.direction = random.choice(direction_list)
    builder.add_object(Exit_[0], Exit_[1], pb.EXIT, pb.GREY)
    builder.add_object(doorR[0], doorR[1], pb.DOOR_CLOSED, pb.RED)
    builder.add_object(keyR[0], keyR[1], pb.KEY, pb.RED)
    builder.add_object(keyB[0], keyB[1], pb.KEY, pb.BLUE)
    builder.add_object(Obj1[0], Obj1[1], pb.BALL, pb.RED)

    for e in walls:
        builder.add_wall(e[1], e[0])

    return result


def generate_level(config):
    if config.HasField('room_size'):
        return generate_rooms(config, config.room_size)


tmp_config = pb.LevelConfig()
tmp_config.room_size.x = 17
tmp_config.room_size.y = 17

# tmp_config.rooms_count.x = 1
# tmp_config.rooms_count.y = 1

lvl = generate_level(tmp_config)


print(base64.b64encode(lvl.SerializeToString()))
data = lvl

# if __name__=="__main__":
#     tmp_config = pb.LevelConfig()
#     tmp_config.rooms.room_size.x = 10
#     tmp_config.rooms.room_size.y = 10
#
#     tmp_config.rooms.rooms_count.x = 1
#     tmp_config.rooms.rooms_count.y = 1
#
#
#     lvl = generate_level(tmp_config)
#
#     print(lvl)
#
#     #
#     #  print(str(lvl))
#     #print(lvl.SerializeToString())
#     print(base64.b64encode(lvl.SerializeToString()))
#     #print(level_size)
#
