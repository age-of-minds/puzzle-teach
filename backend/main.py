import aom_bridge
import puzteach.env_state_pb2
import time

def load_level(name):
  result = game_state_pb2.GameState()

  result.level_size.x = 10
  result.level_size.y = 10

  for x in range(result.level_size.y):
      row = result.rows.add()
      for y in range(result.level_size.x):
        row.cells.add()
  
  return result


class GameBackend():
  def __init__(self):
    print("creating new backend")
    self.env_state = load_level("levels/level_1.json")

  def update(self, user_data):
    result = game_state_pb2.GameStateDelta()
    return result

if __name__ == "__main__":
  server = aom_bridge.launch_dedicated_backend(GameBackend, 9000)
  try:
    while True:
      time.sleep(24*60*60)
  except KeyboardInterrupt:
    server.stop(0)