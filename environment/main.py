import os
import sys
import time
import uuid

import grpc
from grpc_reflection.v1alpha import reflection

from aom_framework import env_service

from concurrent.futures import ThreadPoolExecutor

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../common/python'))

import protos.env_state_pb2 as env_pb
import puzteach.utils as pt_utils
from puzteach.level_generator import generate_level

# A statefull instance of a puzzleteach environment this maps to a single "trial" from the
# user's perspective.
class PuzTeach_env:
  def __init__(self, cfg):
    self.game_state = generate_level(cfg)

  def update(self, user_data, agent_data):
    print("advancing the simulation")
    # Create a delta from the user data 
    delta = env_pb.GameStateDelta()
    delta.actor.CopyFrom(self.game_state.actor)

    action = agent_data.act

    if user_data.HasField('demonstration'):
      # override the agent's move with the demonstration.
      actor_action = user_data.demonstration

    # Apply the avatar's action
    if action == env_pb.ROTATE_LEFT:
      delta.actor.direction = pt_utils.rotate_direction(False, self.game_state.actor.direction)
    elif action == env_pb.ROTATE_RIGHT:
      delta.actor.direction = pt_utils.rotate_direction(True, self.game_state.actor.direction)
    elif action == env_pb.FORWARD:
      new_pos = pt_utils.get_forward_position(self.game_state.actor.position, self.game_state.actor.direction)
  
    # If the avatar is standing on an object, and has his hands free, pick it up.
    # If the object is the exit, then mark the environment as "finished"
    obj_under_actor = pt_utils.get_object_at(self.game_state, delta.actor.position)
    delta.finished = False

    if obj_under_actor:
      if obj_under_actor.type == env_pb.EXIT:
        delta.finished = True
      elif obj_under_actor.type == env_pb.KEY:
        if self.game_state.actor.held_object_id == 0:
          delta.actor.held_object_id = obj_under_actor.object_id
          act = delta.actions.add()
          act.act_type = env_pb.PICKUP
          act.object_id = obj_under_actor.object_id

    # Apply our delta to our internal game state representation.
    pt_utils.advance_inplace(self.game_state, delta)

    return delta

# Implementation of the AoM environment service.
class EnvService(env_service.Servicer):
  def __init__(self):
    # We will be managing a pool of environments, keyed by their session id.
    self._envs = {}
    print("Environment service started")

  # The orchestrator is requesting a new environment
  def Start(self, request, context):
    # The orchestrator will force a session id on to us, but for testing, 
    # it's convenient to be able to create a unique one on demand.
    sess_id = ''
    if request.session_id != '':
      sess_id = request.session_id
    else:
      sess_id = str(uuid.uuid1())
      print("Start: Requestor did not provide a session id, creating one...")
    # Sanity check: We should only ever create a session once.
    if sess_id in self._envs:
      raise Exception("session already exists")

    print(f"spinning up new environment: {sess_id}")

    cfg = env_pb.LevelConfig()
    cfg.ParseFromString(request.user_request.env_specific_data)
    
    # Instantiate the fresh environment
    env = PuzTeach_env(cfg)
    self._envs[sess_id] = env

    # Send the initial state of the environment back to the client (orchestrator, normally.)
    reply = env_service.EnvStartReply(session_id=sess_id)

    reply.initial_sim_state.tick_id = 0
    reply.initial_sim_state.env_specific_data = env.game_state.SerializeToString()

    return reply
  
  # The orchestrator is ready for the environemnt to move forward in time.
  def Update(self, request, context):
    sess_id = request.session_id

    if sess_id not in self._envs:
      raise Exception("session does not exists")

    # Retrieve the environment that matches this session
    env = self._envs[sess_id]

    # Extract the Puzzle-teach specific data from the AoM-generic structures
    user_data = env_pb.UserInput()
    agent_data = env_pb.AgentInput()

    # This is fine for puzzleteach since there is ALWAYS one user and one agent connected.
    user_data.ParseFromString(request.user_data[0].env_specific_data)
    agent_data.ParseFromString(request.agent_data[0].env_specific_data)

    # Advance time
    delta = env.update(user_data, agent_data)

    # Send the delta back to the orchestrator.
    reply = env_service.EnvUpdateReply()
    reply.delta.env_specific_data = delta.SerializeToString()
    
    return reply

def launch_server(port):
  server = grpc.server(ThreadPoolExecutor(max_workers=10))

  env_service.add_servicer_to_server(EnvService(), server)

  SERVICE_NAMES = (
      env_service.SERVICE_NAME,
      reflection.SERVICE_NAME,
  )
  reflection.enable_server_reflection(SERVICE_NAMES, server)

  server.add_insecure_port(f'[::]:{port}')
  server.start()

  return server

if __name__ == '__main__':
  port = os.environ.get('PORT', '9000')
  server = launch_server(port)
  print(f"Environment Service listening on port {port}")
  try:
    while True:
      time.sleep(24*60*60)
  except KeyboardInterrupt:
    server.stop(0)