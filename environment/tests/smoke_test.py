# This test simply connects to a running instance of the environment, 
# starts a session, and updates it 10 times in a row.

import grpc
import os
import sys

from aom_framework import env_service

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../../common/python'))

import protos.env_state_pb2 as env_pb

# Connect
channel = grpc.insecure_channel('localhost:9000')
stub = env_service._service_grpc.EnvironmentStub(channel)

# Start Environment
session = stub.Start(env_service.EnvStartRequest())
sess_id = session.session_id
print(f'session id: {sess_id}')
game_state = env_pb.GameState()
session.initial_sim_state.env_specific_data.Unpack(game_state)

print(game_state)

# Create an update request
update_req = env_service.EnvUpdateRequest(session_id=sess_id)

ai = env_pb.AgentInput(act=env_pb.FORWARD)
ui = env_pb.UserInput()

ad = update_req.agent_data.add()
ud = update_req.user_data.add()

ad.env_specific_data.Pack(ai)
ud.env_specific_data.Pack(ui)

# send it ten times in a row
for i in range(10):
  rep = stub.Update(update_req)
  delta = env_pb.GameStateDelta()
  rep.delta.env_specific_data.Unpack(delta)
  print(delta)

