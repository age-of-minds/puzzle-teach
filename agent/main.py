import os
import sys
import time
import uuid

import grpc
from grpc_reflection.v1alpha import reflection

from aom_framework import agent_service

from concurrent.futures import ThreadPoolExecutor

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../common/python'))

import protos.env_state_pb2 as env_pb
import puzteach.utils as pt_utils

# A statefull instance of a puzzleteach environment this maps to a single "trial" from the
# user's perspective.
class PuzTeach_agent:
  def __init__(self):
    print("initializing agent")
    pass

  def decide_from_state(self, state):
    print("making agent decision")
    result = env_pb.AgentInput()

    # Todo: fill with smarter decision
    result.act = env_pb.ROTATE_RIGHT

    return result

  def reward(self):
    print("rewarding agent")
    pass

# Implementation of the AoM environment service.
class AgentService(agent_service.Servicer):
  def __init__(self):
    print("Agent Service started")
    # We will be managing a pool of agents, keyed by their session id.
    self._agents = {}

  # The orchestrator is requesting a new environment
  def Start(self, request, context):
    print("Agent: Start")
    print(str(request))

    # The orchestrator will force a session id on to us, but for testing, 
    # it's convenient to be able to create a unique one on demand.
    sess_id = request.session_id

    if not sess_id:
      raise Exception("No session ID provided")
      
    # Sanity check: We should only ever create a session once.
    if sess_id in self._agents:
      raise Exception("session already exists")

    
    # Instantiate the fresh agent
    agent = PuzTeach_agent()
    self._agents[sess_id] = (agent, env_pb.GameState())

    # Send the initial state of the environment back to the client (orchestrator, normally.)
    reply = agent_service.AgentStartReply()

    return reply
  
  # The orchestrator is ready for the environemnt to move forward in time.
  def Decide(self, request, context):
    sess_id = request.session_id

    if sess_id not in self._agents:
      raise Exception("session does not exists.")

    # Retrieve the environment that matches this session
    agent, state = self._agents[sess_id]

    if request.HasField('env_state'):
      state.ParseFromString(request.env_state.env_specific_data)

    elif request.HasField('env_delta'):
      delta = env_pb.GameStateDelta()
      delta.ParseFromString(request.env_state_delta.env_specific_data)
      pt_utils.advance_inplace(game_state, delta)
    else:
      raise Exception("no env data.")

    decision = agent.decide_from_state(state)


    # Send the delta back to the orchestrator.
    reply = agent_service.AgentDecideReply()
    reply.decision.env_specific_data = decision.SerializeToString()
    
    return reply

  def Reward(self, request, context):
    sess_id = request.session_id

    if sess_id not in self._agents:
      raise Exception("session does not exists.")

    # Retrieve the environment that matches this session
    agent, state = self._agents[sess_id]

    agent.reward()

    reply = agent_service.AgentRewardReply()

    return reply

def launch_server(port):
  server = grpc.server(ThreadPoolExecutor(max_workers=10))

  agent_service.add_servicer_to_server(AgentService(), server)

  SERVICE_NAMES = (
      agent_service.SERVICE_NAME,
      reflection.SERVICE_NAME,
  )
  reflection.enable_server_reflection(SERVICE_NAMES, server)

  server.add_insecure_port(f'[::]:{port}')
  server.start()

  return server

if __name__ == '__main__':
  port = os.environ.get('PORT', '9000')
  server = launch_server(port)
  print(f"Agent Service listening on port {port}")
  try:
    while True:
      time.sleep(24*60*60)
  except KeyboardInterrupt:
    server.stop(0)