import os
import sys
import time
import grpc
from curses import wrapper

import aom_framework.protocols_pb2 as aom_pbs
import aom_framework.services_pb2_grpc as aom_services

from concurrent.futures import ThreadPoolExecutor

script_location = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_location, '../../common/python'))

import protos.env_state_pb2 as env_pb
import puzteach.utils as pt_utils


def obj_type_to_char(obj_type):
  if obj_type == env_pb.KEY:
    return '🔑'
  if obj_type == env_pb.DOOR_OPENED:
    return 'K'
  if obj_type == env_pb.DOOR_CLOSED:
    return 'K'
  if obj_type == env_pb.BALL:
    return '⚽'
  if obj_type == env_pb.EXIT:
    return 'X'

  return '?'

def direction_to_char(direction):
  if direction == env_pb.TOP:
    return '^'
  if direction == env_pb.BOTTOM:
    return 'v'
  if direction == env_pb.LEFT:
    return '<'
  if direction == env_pb.RIGHT:
    return '>'

  return '?'

def print_state(state, screen):
  w = state.level_size.x
  h = state.level_size.x
  cells = [[' ' for x in range(w)] for y in range(h)] 

  for c in state.cells:
    cells[c.position.x][c.position.y] = '█'
  for o in state.objects:
    cells[o.position.x][o.position.y] = obj_type_to_char(o.type)

  cells[state.actor.position.x][state.actor.position.y] = direction_to_char(state.actor.direction)

  i = 0

  screen.addstr(i,0, '█' * (w+2))
  i += 1

  for line in cells:
    screen.addstr(i, 0, '█' + ''.join(line) + '█')
    i += 1
  screen.addstr(i,0, '█' * (w+2))



def main(stdscr):

   # Clear screen
  stdscr.clear()

  TRIAL_ENDPOINT = 'trials.cluster.puzzleteach.ageofminds.com:80'
    
  channel = grpc.insecure_channel(TRIAL_ENDPOINT)
  trials = aom_services.TrialStub(channel)

  cfg = env_pb.LevelConfig()
  cfg.level_size.x = 17
  cfg.level_size.y = 17
  cfg.room_size.x = 5
  cfg.room_size.y = 5

  trial = trials.Start(aom_pbs.TrialStartRequest(env_specific_data=cfg.SerializeToString()))

  game_state = env_pb.GameState()
  game_state.ParseFromString(trial.initial_sim_state.env_specific_data)

  
  while True:
    # Clear screen
    stdscr.clear()

    print_state(game_state, stdscr)

    stdscr.refresh()
    time.sleep(0.1)

    act =  env_pb.UserInput()
    update = trials.Action(aom_pbs.TrialActionRequest(session_id=trial.session_id, env_specific_data=cfg.SerializeToString()))

    delta = env_pb.GameStateDelta()
    delta.ParseFromString(update.delta.env_specific_data)

    pt_utils.advance_inplace(game_state, delta)

wrapper(main)