# Puzzleteach scripted frontend

This is a fully automated Client for puzzle teach.

It's mainly useful to load-test the backend components, 
but could conceivably also be used for datalog population.