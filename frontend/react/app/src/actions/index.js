import { LOAD_GAME, LOAD_GAME_DELTA, LOAD_SNAPSHOT, SET_PAUSE } from './constants'

export function loadGame(payload) {
    return { type: LOAD_GAME, payload }
};

export function loadGameDelta(payload) {
    return { type: LOAD_GAME_DELTA, payload }
};

export function loadSnapshot(payload) {
    return { type: LOAD_SNAPSHOT, payload }
};

export function setPause(payload) {
    return { type: SET_PAUSE, payload }
};