import { createStore } from "redux";
import rootReducer from "../reducers/index";


const enhancer = window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__();

if (!enhancer) {
    console.warn('Install Redux DevTools Extension to inspect the app state: ' +
        'https://github.com/zalmoxisus/redux-devtools-extension#installation')
}

const store = createStore(
    rootReducer, /* preloadedState, */
    enhancer
);

export default store;