
function get_object_by_id(id, objectsList) {
    return objectsList.filter(x => x.objectId === id)[0]
}

export default { get_object_by_id }