import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 2,

    },

});

class Notification extends Component {

    render() {
        const { classes } = this.props;

        return (
            <div  className={classes.root}>
            </div>
        );
    }

}

export default withStyles(styles)(Notification);
