import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Slider from 'react-rangeslider'
// import store from '../store'
// import { loadGameDelta, setPause } from '../actions'
import { setPause } from '../actions'
// import { backendMock } from '../levels/1'
import SpeedIcon from '../assets/img/SPEED_PICTO.svg'
import PlayIcon from '../assets/img/RESUME.svg'
import PauseIcon from '../assets/img/PAUSE.svg'

import {connection} from "./Connection"
import 'react-rangeslider/lib/index.css'
import './Speed.css'

import { Grid } from '@material-ui/core'
import { connect } from "react-redux";


const styles = {
  root: {
    flexGrow: 1,
  },

  play: {
    background: `url(${PlayIcon}) no-repeat center/100%`,

  },

  pause: {
    background: `url(${PauseIcon}) no-repeat center/100%`,
  },

  speed: {
    background: `url(${SpeedIcon}) no-repeat center/100%`,

  },

  speed_fast: {
    transform: 'scale(-1, 1)',
  }
};

const actionCreators = {
  setPause,
}

class Speed extends React.Component {
  state = {
    value: 50,
    pause: true,
  };

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
    this.loop();
  }

  componentWillUnmount() {
    clearInterval(this.infiniteloop);
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  loop() {
    const { value, pause } = this.state;

    const speed = value * - 20 + 2200

    clearInterval(this.infiniteloop);
    if (!pause) {
     this.infiniteloop = setInterval(
        () => connection.advance(),
        speed
      );
    }
  }

  togglePause = (event) => {
    const { setPause } = this.props;

    const pause = !this.state.pause;

    this.setState({ pause }, function () {
      this.loop();
      setPause(pause)
    });
  };

  handleKeyDown = (event) => {
    if (event.code === 'Space') {
      event.preventDefault();
      this.togglePause();
    }
  }

  handleChange = value => {
    this.setState({ value }, function () {
      this.loop();
    });
  };


  render() {
    const { classes } = this.props;
    const { value, pause } = this.state;

    const playpause = pause ? classes.pause : classes.play;

    return (
      <div className={classes.root}>
        <Grid container >
          <Grid item xs className={playpause} onClick={this.togglePause}></Grid>
          <Grid item xs={10}>
            <Slider
              min={1}
              max={99}
              value={value}
              onChange={this.handleChange}
              tooltip={false}
            />

          </Grid>
          <Grid item xs className={`${classes.speed} ${classes.speed_fast}`}></Grid>
        </Grid>
      </div>
    );
  }
}

Speed.propTypes = {
  classes: PropTypes.object.isRequired,
};

const ConnectedSpeed = connect(null, actionCreators)(Speed);
export default withStyles(styles)(ConnectedSpeed);