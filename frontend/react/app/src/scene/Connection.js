import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import {TrialClient, protos} from 'aom-sdk';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import store from '../store'
import { loadGame, loadGameDelta } from '../actions'

const pt_proto = require('../protos/env_state_pb');

const styles = theme => ({
  root: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: 2,

  }
});

let PENDING = 1
let CONNECTED = 2
let FAILED = 3

class Connection {
  constructor(on_connected) {
    this.client = new TrialClient(process.env.REACT_APP_TRIALS_ENDPOINT);
    this.onConnected = on_connected;
    this.sessionId = null;

    var self = this;
    
    var cfg = new pt_proto.LevelConfig()
    cfg.setLevelSize(new pt_proto.Vec2([17, 17]))
    cfg.setRoomSize(new pt_proto.Vec2([5, 5]))

    var start_req = new protos.TrialStartRequest();    
    start_req.setEnvSpecificData(cfg.serializeBinary())

    this.client.start(start_req, {}, (err, rep) => {
      window.setTimeout(()=>{
        if( err) {
          console.log("failed to connect: " + err.code + " : " + err.message);
          self.onConnected(FAILED);
        }
        else {
          self.onConnected(CONNECTED);
          self.sessionId = rep.getSessionId(); 
          console.log(rep.toObject());
          const init_game_state = pt_proto.GameState.deserializeBinary(rep.getInitialSimState().getEnvSpecificData());
  
          store.dispatch(loadGame(init_game_state));
          console.log(init_game_state.toObject());
        }
      }, 1000);
    });
  }

  advance = () => {
    var action = new protos.TrialActionRequest();
    action.setSessionId(this.sessionId);
    this.client.action(action, {}, (err, rep) => {
      if( err) {
        console.log("failed to advance: " + err.code + " : " + err.message);
      }
      else {
        console.log(rep.toObject());
        const delta = pt_proto.GameStateDelta.deserializeBinary(rep.getDelta().getEnvSpecificData());
        store.dispatch(loadGameDelta(delta));
      }
    });
  }
}

export var connection = null;

class ConnectionState extends Component {
  constructor(props) {
    super(props);
    var self = this;

    this.state = {
      conn_state: PENDING,
      hasStarted: false
    };

    connection = new Connection(state => {
      self.setState({conn_state: state});
    });

  }

  handleClose = () => {
    this.setState({ hasStarted: true });
  };

  render() {
    let msg;

    if(!this.state.hasStarted) {
      let msg_text = "...";
      if(this.state.conn_state === CONNECTED) {
        msg_text = "Ready!";
      }
      else if(this.state.conn_state === FAILED) {
        msg_text = "Connection failed :( try again later."
      }

      let disable_btn = this.state.conn_state !== CONNECTED;
      msg = <Dialog
        fullScreen={false}
        disableBackdropClick={true}
        open={true}
        onClose={this.handleClose}
        aria-labelledby="responsive-dialog-title"
      >
      <DialogTitle id="responsive-dialog-title">{"Connecting you to an agent."}</DialogTitle>
      <DialogContent>
          <DialogContentText>{msg_text}</DialogContentText>
      </DialogContent>
      <DialogActions>
          <Button 
          disabled={disable_btn}
          onClick={this.handleClose} color="primary" autoFocus>
              Let's go
          </Button>
      </DialogActions>
    </Dialog>
    }

    return (<div>{msg}</div>);
  }
}  

export default withStyles(styles)(ConnectionState);
