import React, { Component } from 'react';
import './Grid'
import Grid from './Grid';
import Mode from './Mode';
import Notification from './Notification'
import ConnectionState from './Connection'

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        position: 'relative',
    },
});

const mapStateToProps = state => {
    return {
        pause: state.pause,
    };
};



class Scene extends Component {
    state = {
        isLoggedIn: false
    }

    handleClose = () => {
        sessionStorage.setItem('isLoggedIn', true)
        this.setState({ isLoggedIn: true });

    };

    render() {
        const { classes, fullScreen, pause } = this.props;

        let welcome;
        if (!sessionStorage.getItem('isLoggedIn')) {
            welcome =
                <Dialog
                    fullScreen={fullScreen}
                    disableBackdropClick={true}
                    open={!this.state.isLoggedIn}
                    onClose={this.handleClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{"Welcome to puzzle-teach"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Are you ready ?
                </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Let's play
                        </Button>
                    </DialogActions>
                </Dialog>
        }

        let is_notification;
        if (pause) {
            is_notification = <Notification />
        }

        return (
            <div className={classes.root}>
                {is_notification}
                {welcome}
                <ConnectionState/>
                <Mode>
                    <Grid />
                </Mode>

            </div>

        );
    }
}

const ConnectedScene = connect(mapStateToProps)(Scene);
export default withStyles(styles)(ConnectedScene);
