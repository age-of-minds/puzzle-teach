import React, { Component } from 'react';

import Carousel from "nuka-carousel";
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import Action from './Action'

const styles = theme => ({
    root: {
        width: "100%",
        height: "100px",
    },
});

const mapStateToProps = state => {
    return {
        actions: [...state.actionsList],
    };
};

class Timeline extends Component {

    state = {
        wrapAround: false,
        underlineHeader: false,
        slidesToShow: 6,
        cellAlign: "right",
        transitionMode: "scroll",
        heightMode: "max",
        withoutControls: true
    };


    render() {
        const { classes, actions } = this.props;

        let actionsLogo = actions.map((item, key) => {
            return (
                <Action 
                picture={item.picture}
                key={key}
                position={key}
                snapshot={item.snapshot}
                />
            )
        })

        const length = actions.length;

        return (
            <div className={classes.root}>
                <Carousel
                    length={length}
                    className={classes.timeline}
                    withoutControls={this.state.withoutControls}
                    transitionMode={this.state.transitionMode}
                    cellAlign={this.state.cellAlign}
                    slidesToShow={this.state.slidesToShow}
                    wrapAround={this.state.wrapAround}
                    slideIndex={length - 1 }
                    heightMode={this.state.heightMode}
                >

                    {actionsLogo}

                </Carousel>
            </div>
        )
    }
}

const ConnectedTimeline = connect(mapStateToProps)(Timeline);
export default withStyles(styles)(ConnectedTimeline);