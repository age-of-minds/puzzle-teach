import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { loadSnapshot } from '../../actions'

const styles = theme => ({
    picto: {
        height: "100px",
    }
});


const actionCreators = {
    loadSnapshot,
}

class Action extends Component {

    render() {
        const { classes, picture, snapshot, loadSnapshot, position} = this.props;

        return (
            <img className={classes.picto} onClick={() => loadSnapshot({snapshot, position})} alt={""} src={picture} />
        )

    }

}

const ConnectedAction = connect(null, actionCreators)(Action);
export default withStyles(styles)(ConnectedAction);
