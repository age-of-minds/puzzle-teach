import React, { Component } from 'react';
import { default as UiGrid } from '@material-ui/core/Grid';
import Player from '../objects/Player';
import Item from '../objects/Item';
import './Tile.css'
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
const proto = require('../protos/env_state_pb');

const styles = theme => ({
    tile: {
        backgroundSize: 'cover',
        position: 'relative',
    },
});

const mapStateToProps = state => {
    return {
        player: state.actor,
        // items: state.objectsList || [],

    };
};

class Tile extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    class_from_type(type) {
        const types = proto.CellType;
        const result = Object.keys(types).find(key => key === type);
        return result.toLowerCase();
    }

    handleClick(e) {
        e.stopPropagation();

        console.log(`${this.props.x}, ${this.props.y}`);
    };

    render() {
        const { classes, type, x, y, player, items } = this.props;

        let is_player_on;
        if (player.position.x === x && player.position.y === y) {
            is_player_on = <Player direction={player.direction} />
        }

        let itemsState = items.map((item, key) => {
            return <Item key={key} type={item.type} color={item.color} />
        })

        let cell_type='empty';
        if (type){
            cell_type = this.class_from_type(type)
        }

        return (
            <UiGrid onClick={this.handleClick} className={`${cell_type} ${classes.tile}`} item xs>
                {itemsState}
                {is_player_on}
            </UiGrid>
        );
    }
}

const ConnectedTile = connect(mapStateToProps)(Tile);
export default withStyles(styles)(ConnectedTile);
