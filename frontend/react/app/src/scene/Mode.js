import React, { Component } from 'react';
import { default as UiGrid } from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    mode: {
        flexGrow: 1,
        width: '80vw',
        height: '80vw',
        margin: '0 auto',
        padding: '4vw',

        '@media (min-width:100vh)': {
            height: '80vh',
            width: '80vh',
        },
    },

    demo: {
        backgroundColor: 'red',
    },

    inductive: {
        backgroundColor: 'yellow',
    },

    label: {
        position: 'absolute',
        top:0,
        left:0,
        width: '100%',
        textAlign: 'center',
        fontSize: '1.4rem',
    }

})

class Mode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            demo: true,
        };

        this.toggleDemo = this.toggleDemo.bind(this);
    }

    toggleDemo(e) {
        this.setState(state => ({
            demo: !state.demo
        }));

        console.log('The link was clicked.');
    };

    render() {
        const { classes } = this.props;

        const styleMode = this.state.demo ? classes.demo : classes.inductive;
        const content = this.state.demo ? 'DEMO MODE' : 'INDICATIVE MODE'

        return (
            <UiGrid container onClick={this.toggleDemo} className={`${styleMode} ${classes.mode}`}>
                <p className={classes.label}>{content}</p>
                {this.props.children}
            </UiGrid>

        );
    }
}

export default withStyles(styles)(Mode);
