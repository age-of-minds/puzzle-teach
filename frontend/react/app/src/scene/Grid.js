import React, { Component } from 'react';
import { default as UiGrid } from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Tile from './Tile';
import './Grid.css'
// import { keyframes } from "styled-components";
import punishIcon from '../assets/img/PUNISH.svg'
import rewardIcon from '../assets/img/REWARD.svg'
import { connect } from "react-redux";

const styles = theme => ({
    grid: {
        flexGrow: 1,
        border: '1px solid red',
        backgroundColor: '#fff',
        position: 'relative',
    },
    row: {
        flexGrow: 1,
        flexDirection: 'row',
        flexBasis: 0,
    },
    control: {
        position: 'absolute',
        bottom: '-120px',
        textAlign: 'center',
    },

    reward: {
        height: '100px',
        cursor: 'pointer',
    },

    highlight: {
        position: 'absolute',
        zIndex: '1',
        width: "100%",
        height: "100%",
        animation: "highlight-up 0.3s"
    },

    ko: {
        backgroundColor: 'red',
    },

    ok: {
        backgroundColor: 'green',
    },
});

const mapStateToProps = state => {
    return {
        level_size: state.levelSize,
        cells: state.cellsList || [],
        items: state.objectsList || [],
    };
};

class Grid extends Component {

    state = {
        flash: null,
    };

    buildItemsIndex(items) {
        let idx = [];

        items.forEach((item, key) => {
            if (idx[item.position.x] === undefined) {
                idx[item.position.x] = [];
            }
            if (idx[item.position.x][item.position.y] === undefined) {
                idx[item.position.x][item.position.y] = [];
            }

            idx[item.position.x][item.position.y].push(key)

        })
        return idx;
    }

    buildWallsIndex(cells) {
        let idx = [];

        cells.forEach((cell, key) => {
            // console.log(cell, key)
            if (idx[cell.position.x] === undefined) {
                idx[cell.position.x] = [];
            }

            if (idx[cell.position.x][cell.position.y] === undefined) {
                idx[cell.position.x][cell.position.y] = [];
            }

            idx[cell.position.x][cell.position.y] = 'wall'

        })

        return idx;
    }

    hl(e, flash) {
        e.stopPropagation();
        this.setState({ flash });
        setTimeout(() => { this.setState({ flash: null }) }, 200);
    }



    render() {
        const { classes, level_size, cells, items } = this.props;
        const { flash } = this.state;

        if (!level_size) {
            return <div />
        }

        const itemsIdx = this.buildItemsIndex(items);
        const wallsIdx = this.buildWallsIndex(cells);
        // console.log(wallsIdx);

        let is_flash
        if (flash) {
            const klass = classes.highlight
            is_flash = <div className={`${classes[flash]} ${klass}`}></div>
        }

        return (
            <UiGrid container direction='column' className={classes.grid}>

                {is_flash}

                {[...Array(level_size.y).keys()].map((row, r_index) => (
                    <UiGrid container className={classes.row} key={`row${r_index}`} justify="center">

                        {[...Array(level_size.x).keys()].map((column, c_index) => {

                            let active_items = [];
                            if (itemsIdx[c_index] && itemsIdx[c_index][r_index]) {
                                const itemsId = itemsIdx[c_index][r_index];
                                active_items = itemsId.map(id => { return items[id] })
                            }

                            let cell_type;
                            if (wallsIdx[c_index] && wallsIdx[c_index][r_index]) {
                                cell_type = 'WALL';
                            }

                            return <Tile x={c_index} y={r_index} items={active_items} type={cell_type} key={`cell_${c_index}_${r_index}`} />

                        })}

                    </UiGrid>
                ))}

                <UiGrid container className={classes.control}>
                    <UiGrid item xs><img alt="punish" onClick={(e) => this.hl(e, 'ko')} className={classes.reward} src={punishIcon} /></UiGrid>
                    <UiGrid item xs><img alt="reward" onClick={(e) => this.hl(e, 'ok')} className={classes.reward} src={rewardIcon} /></UiGrid>
                </UiGrid>

            </UiGrid >
        );
    }
}


const ConnectedGrid = connect(mapStateToProps)(Grid);
export default withStyles(styles)(ConnectedGrid);