import helper from './helper';

const proto = require('../protos/env_state_pb');

const gameState = new proto.GameState();
gameState.setLevelSize(helper.coord(5, 5))

const playerState = new proto.ActorState()
playerState.setPosition(helper.coord(2, 2))
playerState.setDirection(proto.Direction.UP)
gameState.setActor(playerState)

export default gameState
