const proto = require('../protos/env_state_pb');


function cellState(type = proto.CellType.EMPTY) {
    let cell = new proto.CellState();
    cell.setType(type)

    return cell;
}

export function addWalls(walls) {
    return walls.map((position) => {
        const cell = cellState(proto.CellType.WALL);
        cell.setPosition(coord(position[0], position[1]));
        return cell;
    });
}

export function buildObjects(type, objects, start_id = 1) {

    return objects.map((item) => {
        const position = item[0];
        const color = item[1];

        const objectState = new proto.ObjectState();
        objectState.setObjectId(start_id++);
        objectState.setType(type);
        objectState.setPosition(coord(position[0], position[1]));
        objectState.setColor(color);
        return objectState;
    });
}

export function coord(x, y) {
    return new proto.Vec2([x, y])
}

export function* moveGenerator(moves) {
    let index = 0;
    while (index < moves.length) {
        yield moves[index];
        index++
        if (index === moves.length) {
            index = 0;
        }
    }
}

export function base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

export function GameStateFromPayload(payload) {
    try {
        const result = base64ToArrayBuffer(payload)
        const gameState = proto.GameState.deserializeBinary(result)
        console.log('gs', gameState)
        return gameState
    }
    catch (error) {
        console.warn('payload invalid')
    }

    return false

}

export default { addWalls, buildObjects, coord, moveGenerator, base64ToArrayBuffer, GameStateFromPayload }