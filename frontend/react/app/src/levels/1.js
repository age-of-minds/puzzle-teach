import helper from './helper';

const proto = require('../protos/env_state_pb');

const rows = 10;
const columns = rows;

const walls = [
    [0, 0], [1, 0], [1, 4], [0, 2], [1, 2], [3, 3]
];

// TODO: maybe use explicit struct
const balls = [
    [[1, 9], proto.Color.BLUE],
    [[4, 3], proto.Color.RED],
    [[4, 5], proto.Color.BLUE],
];

const doorsOpened = [
    [[6, 3], proto.Color.BLUE]
];

const doorsClosed = [
    [[6, 6], proto.Color.BLUE]
];

const keys = [
    [[8, 6], proto.Color.BLUE]
];

const exit = [
    [[9, 0], proto.Color.GREY]

]

const gameState = new proto.GameState();

gameState.setLevelSize(helper.coord(rows, columns))

const playerState = new proto.ActorState()
playerState.setPosition(helper.coord(0, columns - 1))
playerState.setDirection(proto.Direction.RIGHT)
gameState.setActor(playerState)

// helper.initGrid(gameState, rows, columns);
gameState.setCellsList(helper.addWalls(walls))


let items = []
items.push(...helper.buildObjects(proto.ObjectType.BALL, balls));
items.push(...helper.buildObjects(proto.ObjectType.DOOR_OPENED, doorsOpened, items.length));
items.push(...helper.buildObjects(proto.ObjectType.DOOR_CLOSED, doorsClosed, items.length));
items.push(...helper.buildObjects(proto.ObjectType.KEY, keys, items.length));
items.push(...helper.buildObjects(proto.ObjectType.EXIT, exit, items.length));


items.forEach(state => { gameState.addObjects(state) })


// Mock backend
let moves = []

const gsDelta1 = new proto.GameStateDelta();
const ps1 = playerState.cloneMessage();
ps1.setPosition(helper.coord(1, 9));
ps1.setHeldObjectId(1);
const playerAction = new proto.ActorAction()
playerAction.setActType(proto.ActionType.PICKUP)
playerAction.setObjectId(1)
gsDelta1.setActionsList([playerAction])
gsDelta1.setActor(ps1);
moves.push(gsDelta1);


const gsDelta2 = new proto.GameStateDelta();
const ps2 = ps1.cloneMessage();
ps2.setPosition(helper.coord(2, 9));


gsDelta2.setActor(ps2);
const osDelta1 = new proto.ObjectStateDelta();
osDelta1.setChange(proto.ChangeType.UPDATED);
const oNewState1 = items[0].cloneMessage();
oNewState1.setPosition(ps2.getPosition());
osDelta1.setNewState(oNewState1);
gsDelta2.setObjectsList([osDelta1]);
moves.push(gsDelta2);




const gsDelta3 = new proto.GameStateDelta();
const ps3 = ps2.cloneMessage();
ps3.setPosition(helper.coord(3, 9));
gsDelta3.setActor(ps3);
const osDelta2 = osDelta1.cloneMessage();
const oNewState2 = oNewState1.cloneMessage();
oNewState2.setPosition(ps3.getPosition());
osDelta2.setNewState(oNewState2);
gsDelta3.setObjectsList([osDelta2]);
moves.push(gsDelta3);



const gsDelta4 = new proto.GameStateDelta();
const ps4 = ps3.cloneMessage();
ps4.setPosition(helper.coord(3, 8));
ps4.setDirection(proto.Direction.TOP);

const playerAction4 = new proto.ActorAction()
playerAction4.setActType(proto.ActionType.DROP)
playerAction4.setObjectId(1)
ps4.setHeldObjectId(0)
gsDelta4.setActionsList([playerAction4])
gsDelta4.setActor(ps4);

moves.push(gsDelta4);

const gsDelta5 = new proto.GameStateDelta();
const ps5 = ps4.cloneMessage();
ps5.setPosition(helper.coord(3, 7));
gsDelta5.setActor(ps5);
moves.push(gsDelta5);

const gsDelta6 = new proto.GameStateDelta();
const ps6 = ps5.cloneMessage();
ps6.setPosition(helper.coord(3, 6));
gsDelta6.setActor(ps6);
moves.push(gsDelta6);

export const backendMock = helper.moveGenerator(moves);



export default gameState
