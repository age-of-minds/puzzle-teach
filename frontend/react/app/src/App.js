import React, { Component } from 'react';
import './App.css';
import Scene from './scene'
import blank_game from './levels/blank'
import store from './store'
import { loadGame } from './actions'
import { withStyles } from '@material-ui/core/styles';

import Speed from './scene/Speed';
import Timeline from './scene/Timeline/Timeline';

import { Grid } from '@material-ui/core';




const styles = {
  root: {
    margin: '20px',
  },

  slider: {
    margin: '20px 0',
  },
  input: {
    width: '100%',
  }
};


class App extends Component {

  constructor(props) {
    super(props);
   
    store.dispatch(loadGame(blank_game));
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>

        <Speed />

        <Grid container justify="center">
          <Scene />
        </Grid>

        <Grid container>
          <Timeline />

        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(App);
