import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import player from '../assets/img/PLAYER.svg'
import protoHelp from './../protos/helper'

const styles = theme => ({
    player: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: '0',
        left: '0',
    },

    top: {

    },
    right: {
        transform: 'rotate(90deg)'
    },
    bottom: {
        transform: 'rotate(180deg)'
    },
    left: {
        transform: 'rotate(-90deg)'
    }

});

const mapStateToProps = state => {
    return {
        direction: state.actor.direction,
    };
};

class Player extends Component {

    render() {
        const { classes, direction } = this.props;
        const direction_str = protoHelp.direction(direction).toLowerCase()

        return (
            <div className={`${classes.player} ${classes[direction_str]}`} style={{ backgroundImage: `url(${player})` }}></div>
        );
    }
}


const ConnectedPlayer = connect(mapStateToProps)(Player);
export default withStyles(styles)(ConnectedPlayer);