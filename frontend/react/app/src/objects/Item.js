import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import protoHelp from './../protos/helper'

const styles = theme => ({
    item: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: '0',
        left: '0',
    },

});


class Item extends Component {


    render() {
        const { classes, type, color } = this.props;
        const type_str = protoHelp.type(type)
        const color_str = protoHelp.color(color)

        const img = require(`../assets/img/${type_str}_${color_str}.svg`);

        return (
            <div className={`${classes.item}`} style={{ backgroundImage: `url(${img})` }}></div>
        );
    }
}


export default withStyles(styles)(Item);