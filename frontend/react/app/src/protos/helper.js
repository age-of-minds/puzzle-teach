const proto = require('./env_state_pb');

function actionType(id) {
    const types = proto.ActionType;
    const result = Object.keys(types).find(key => types[key] === id);
    return result;
}


function type(id) {
    const types = proto.ObjectType;
    const result = Object.keys(types).find(key => types[key] === id);
    return result;
}

function direction(direction) {
    const directions = proto.Direction;
    const result = Object.keys(directions).find(key => directions[key] === direction);
    return result;
}


function color(id) {
    const colors = proto.Color;
    const result = Object.keys(colors).find(key => colors[key] === id);
    return result;
}


export default { type, direction, color, actionType }