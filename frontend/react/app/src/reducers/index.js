import { LOAD_GAME, LOAD_GAME_DELTA, LOAD_SNAPSHOT, SET_PAUSE } from '../actions/constants'
import storeHelper from '../store/helper'
import protoHelp from '../protos/helper'
const proto = require('../protos/env_state_pb');


const initialState = {
    objectsList: [],
    actionsList: [],
    pause: false,
};


function build_actions(actions, objectsList, state) {
    return actions.map(action => {
        let new_action = action.toObject()

        const { type, color } = storeHelper.get_object_by_id(action.getObjectId(), objectsList)

        const type_str = protoHelp.type(type)
        const act_type_str = protoHelp.actionType(action.getActType())
        const color_str = protoHelp.color(color)

        new_action.picture = require(`../assets/img/${act_type_str}_${type_str}_${color_str}.svg`);

        new_action.snapshot = {
            actor: state.actor,
            objectsList: [...state.objectsList],
            // cellsList: [...state.cellsList],
        }

        // Use JSON for deep copy of objects into snapshot
        return JSON.parse(JSON.stringify(new_action))
    })

}

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_GAME:
            return Object.assign({}, state, action.payload.toObject());

        case LOAD_GAME_DELTA: {

            // console.log(action.payload);
            let newState = {
                objectsList: [...state.objectsList],
                actionsList: [...state.actionsList],
            }

            if (action.payload.getActionsList().length) {
                newState.actionsList = newState.actionsList.concat(build_actions(action.payload.getActionsList(), newState.objectsList, state))
            }

            if (action.payload.getActor()) {
                newState.actor = action.payload.getActor().toObject();
            }

            action.payload.getObjectsList().forEach(obj => {
                if (obj.getChange() === proto.ChangeType.UPDATED) {
                    let old_object = storeHelper.get_object_by_id(obj.getNewState().getObjectId(), newState.objectsList);
                    Object.assign(old_object, obj.getNewState().toObject());

                }
                else if (obj.getChange() === proto.ChangeType.ADDED) {
                    // TODO: implement
                    console.warn("proto.ChangeType.ADDED need to be implemented")

                }
                else if (obj.getChange() === proto.ChangeType.REMOVED) {
                    // TODO: implement
                    console.warn("proto.ChangeType.REMOVED need to be implemented")
                }

            })


            return Object.assign({}, state, newState);

        }

        case LOAD_SNAPSHOT: {
            // console.log("load", action.payload)
            let newState = action.payload.snapshot
            const position = action.payload.position

            newState.actionsList = position ? state.actionsList.slice(0, position) : []

            return Object.assign({}, state, newState);

        }

        case SET_PAUSE: {
            return Object.assign({}, state, { pause: action.payload });

        }

        default:
            console.warn(`${action.type} not implemented`)
            return state;
    };




};



export default rootReducer;