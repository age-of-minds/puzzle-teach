# puzzle-teach

A simple puzzle solving environment, heavily based on MILA's baby-ai: https://github.com/mila-udem/babyai

## Requirements

Install make, docker and docker-compose

## Running Puzzle-teach locally

From the project's root:

```
docker run -v $(pwd)/frontend/react/app:/app --rm node:8.15-alpine yarn --cwd /app install
docker-compose build
docker-compose up frontend
```

If you need to see logs from agent and env use `docker-compose up frontend env agent` instead

### Using a custom framework

**Custom orchestrator**
1. Modify `docker-compose.yaml` So that the orchestrator's image points to" `registry.gitlab.com/age-of-minds/aom-framework/aom-orchestrator:latest`
2. From the aom-framework directory, run `make orchestrator`

**Custom javascript sdk**
1. Modify package.json so that aom-sdk points to `file:/aom/sdk_js`
2. Run the following command: `docker run -v /path/to/aom-framework/:/aom -v $(pwd)/frontend/react/app:/app --rm node:8.15-alpine yarn --cwd /app install`

**Custom python sdk**

TBD

**Install contour as a cluster admin**
```
cd k8s/contour
kubectl apply -f .
```

**Install cert manager as a cluster admin**

https://docs.cert-manager.io/en/latest/getting-started/install.html

**Generate certificate**

create a file secret-access-key and paste your aws secret key
```
kubectl create secret generic cert-manager-aws -n=cert-manager --from-file=secret-access-key
```

replace aws access key in cert/issuer.yaml

```
cd cert
kubectl apply -f issuer.yaml
kubectl apply -f certificate.yaml
```

**Logging**

Namespace `logging` is created by terraform in efk.tf

```
# https://github.com/elastic/helm-charts/tree/master/elasticsearch
helm repo add elastic https://helm.elastic.co
helm upgrade --install --namespace=logging --force --wait -f ../charts/elasticsearch.yml elasticsearch elastic/elasticsearch
```

```
helm repo add kiwigrid https://kiwigrid.github.io
helm upgrade --install --namespace=logging --force --wait -f ../charts/fluentd.yml fluentd-es kiwigrid/fluentd-elasticsearch
```

```
helm upgrade --install --namespace=logging --force --wait -f ../charts/kibana.yml kibana stable/kibana 
```


