resource "aws_iam_user" "cert-manager" {
  name = "cert-manager"
  path = "/"
}

resource "aws_iam_access_key" "cert-manager" {
  user = "${aws_iam_user.cert-manager.name}"
}

resource "aws_iam_user_policy" "cert-manager-policy" {
  name = "cert-manager-policy"
  user = "${aws_iam_user.cert-manager.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "route53:GetChange",
            "Resource": "arn:aws:route53:::change/*"
        },
        {
            "Effect": "Allow",
            "Action": "route53:ChangeResourceRecordSets",
            "Resource": "arn:aws:route53:::hostedzone/*"
        },
        {
            "Effect": "Allow",
            "Action": "route53:ListHostedZonesByName",
            "Resource": "*"
        }
    ]
}
EOF
}
