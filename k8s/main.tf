terraform {
  backend "s3" {
    shared_credentials_file = "~/.aws/credentials"
    profile                 = "puzzleteach"
    bucket                  = "puzzle-teach.k8s.state"
    key                     = "terraform.tfstate"
    region                  = "us-east-1"
    dynamodb_table          = "terraform-lock-table"
  }
}

resource "kubernetes_namespace" "puzzleteach" {
  metadata {
    name = "puzzleteach"
  }
}

resource "kubernetes_service_account" "tiller" {
  metadata {
    name = "tiller",
    namespace = "${kubernetes_namespace.puzzleteach.id}"
  }
}
resource "kubernetes_role" "tiller-manager" {
  metadata {
    name = "tiller-manager"
    namespace = "${kubernetes_namespace.puzzleteach.id}"
  }
  rule {
    api_groups = ["", "extensions", "apps"] 
    resources  = ["deployments", "replicasets", "services", "pods", "pods/portforward", "configmaps", "namespaces", "serviceaccounts", "ingresses"]
    verbs      = ["*"]
  }
}
resource "kubernetes_role_binding" "tiller-binding" {
  metadata {
    name = "tiller-binding"
    namespace = "${kubernetes_namespace.puzzleteach.id}"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role" 
    name = "${kubernetes_role.tiller-manager.metadata.0.name}"
    namespace = "${kubernetes_namespace.puzzleteach.id}"
  }
  subject {
    kind = "ServiceAccount"
    name = "${kubernetes_service_account.tiller.metadata.0.name}"
    namespace = "${kubernetes_namespace.puzzleteach.id}"
  }
}