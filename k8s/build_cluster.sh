#!/bin/bash

# export AWS_PROFILE=puzzleteach-kops
export KOPS_STATE_STORE=s3://puzzle-teach.k8s.state
export NAME=cluster.puzzleteach.ageofminds.com
export ZONES="us-east-1a,us-east-1b,us-east-1c"

kops create cluster ${NAME} \
	--state=${KOPS_STATE_STORE} \
	--master-count 1 \
	--master-size t3.medium \
	--node-count 2 \
    --node-size t3.small \
	--zones ${ZONES} \
	--topology private \
	--networking weave \
	--admin-access 142.118.19.42/32 \
	--ssh-public-key ~/.ssh/id_rsa.puzzleteach.pub \
	--ssh-access 142.118.19.42/32 \
	--out=. \
    --target=terraform


